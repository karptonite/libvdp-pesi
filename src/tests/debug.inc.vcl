sub vcl_recv {
        pesi_debug.register_privs();
}
sub vcl_deliver {
        pesi_debug.check_privs();
}
sub vcl_synth {
        # return(fail) implies a rollback - PRIVs lost
        if (resp.status != 503 || resp.reason != "VCL failed") {
                pesi_debug.check_privs();
        }
}
