varnishtest "Test req_top.* in an ESI context"

varnish v1 -arg "-p feature=+esi_disable_xml_check" \
	   -errvcl {Variable is read only.} {
	sub vcl_recv {
		set req_top.url = "/foo";
	}
}

server s1 {
	rxreq
	expect req.http.top-url == "/"
	expect req.http.top-method == "GET"
	expect req.http.top-proto == "HTTP/1.1"
	expect req.http.top-foo == "bar"
	txresp -body {
		<html>
		Before include
		<esi:include src="/a"/>
		<esi:include src="/b"/>
		After include
		</html>
	}
} -start

server s2 {
	rxreq
	expect req.url == "/a1"
	expect req.http.top-url == "/"
	expect req.http.top-method == "GET"
	expect req.http.top-proto == "HTTP/1.1"
	expect req.http.top-foo == "bar"
	txresp -body {
		Included file
		<esi:include src="/c"/>
	}
} -start

server s3 {
	rxreq
	expect req.url == "/c2"
	expect req.http.top-url == "/"
	expect req.http.top-method == "GET"
	expect req.http.top-proto == "HTTP/1.1"
	expect req.http.top-foo == "bar"
	txresp
} -start

server s4 {
	rxreq
	expect req.url == "/b1"
	expect req.http.top-url == "/"
	expect req.http.top-method == "GET"
	expect req.http.top-proto == "HTTP/1.1"
	expect req.http.top-foo == "bar"
	txresp
} -start

varnish v1 -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_recv {
		if (req.esi_level > 0) {
			set req.url = req.url + req.esi_level;
		} else {
			set req.http.foo = "bar";
		}

		set req.http.top-url = req_top.url;
		set req.http.top-method = req_top.method;
		set req.http.top-proto = req_top.proto;
		set req.http.top-foo = req_top.http.foo;
	}
	sub vcl_backend_response {
		set beresp.do_esi = true;
	}

	sub vcl_backend_fetch {
		if (bereq.url == "/") {
			set bereq.backend = s1;
		}
		elsif (bereq.url == "/a1") {
			set bereq.backend = s2;
		}
		elsif (bereq.url == "/c2") {
			set bereq.backend = s3;
		}
		elsif (bereq.url == "/b1") {
			set bereq.backend = s4;
		}
	}

	sub vcl_deliver {
		pesi.activate();
	}
} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.bodylen == 81
	expect resp.body == {
		<html>
		Before include
		
		Included file
		
	
		
		After include
		</html>
	}
} -run

varnish v1 -expect esi_errors == 0

## HTTP/2

varnish v1 -cliok "param.set feature +http2"

client c1 {
	stream 1 {
		txreq
		rxresp
		expect resp.status == 200
		expect resp.bodylen == 81
		expect resp.body == {
		<html>
		Before include
		
		Included file
		
	
		
		After include
		</html>
	}
	} -run
} -run
